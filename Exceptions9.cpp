#include <stdio.h>
#include <exception>
#include <chrono>
#include <string>
/*
Write a function that is called by itself by 100 times in depth. Give
this function an argument that determines at which level an exception is thrown.
Have main() catch these exceptions and print out depth level.
Modify the program to measure if there is a difference in the cost of
catching exceptions depending on where in the function call stack the exception
is thrown. Add a string object to each function and measure again.
*/
struct MyException : public std::exception {
	int level;
	MyException(int _level) {
		level = _level;
	}
};
void function(int current_lvl, int exception_lvl) {
	std::string st = "Test string";
	if (current_lvl == exception_lvl) {
		throw MyException(current_lvl);
	}
	else {
		function(--current_lvl, exception_lvl);
	}
}
void calculateTime(int exceptionDepth) {
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();
	try {
		function(100, exceptionDepth);
	}
	catch (MyException e) {
	}
	end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end - start;
	printf("Depth %d time: %lf\n", 100 -
		exceptionDepth, elapsed_seconds.count());
}
void main() {
	for (int i = 0; i < 100; i++) {
		calculateTime(i);
	}
}
