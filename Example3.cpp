#include <iostream>


struct data {
	short a;
	short b;
};

int main() {
	long value = 0xA2345678;
	data* pdata = reinterpret_cast<data*> (&value);
	std::cout << pdata->a << std::endl;
	return 0;
}