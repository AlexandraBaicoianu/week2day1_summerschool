struct Base { };

struct Derived : Base { };

int main()
{
	Base b;
	Derived d;

	Base *pb = dynamic_cast<Base*>(&d);      	// #1 is always successful when we cast a class to one of its base classes 
	Derived *pd = dynamic_cast<Derived*>(&b); 	// #2 base-to-derived conversions are not allowed with dynamic_cast unless the base class is polymorphic

	return 0;
}

//we make the Base class polymorphic by adding virtual function as in the code sample below, it will be compiled successfully.
//But at runtime, the #2 cast fails and produces null pointer