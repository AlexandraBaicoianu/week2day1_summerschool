#include <string>
#include <iostream>
int main()
{
	int number;
	bool done = false;
	std::string str =
		"The input stream is in the fail state.";
	do
	{
		try
		{
			std::cout << "Enter an integer: ";
			std::cin >> number;
			std::cout << std::endl;
			if (!std::cin)
				throw str;
			done = true;
			std::cout << " Number = " << number
				<< std::endl;
		}
		catch (std::string messageStr)
		{
			std::cout << "Line in string catch" << messageStr
				<< std::endl;
			std::cout << "Restoring the "
				<< "input stream." << std::endl;
			std::cin.clear();
			std::cin.ignore(100, '\n');
		}
	} while (!done);
	return 0;
}

