#include <iostream>
#include <typeinfo> 

int main()
{
	short a(4);
	short b(5);
	std::cout << typeid(a + b).name() << " " << a + b << std::endl; 

	return 0;
}

/* what about this?

#include <iostream>
#include <typeinfo> 

int main()
{
double d(4.0);
short s(2);
std::cout << typeid(d + s).name() << " " << d + s << std::endl; 

return 0;
}
*/