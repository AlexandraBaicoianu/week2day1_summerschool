#include <iostream>

struct A {};
struct B {};
struct C : A, B {};

int main()
{
	C d;
	A *a = &d;
	B *b = &d;

	bool bool1 = reinterpret_cast<char*>(a) == reinterpret_cast<char*>(&d);
	bool bool2 = b == &d;
	bool bool3 = reinterpret_cast<char*>(a) == reinterpret_cast<char*>(b);

	std::cout << bool1 << bool2 << bool3 << std::endl; 

	return 0;
}