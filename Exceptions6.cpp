#include <iostream>
#include <string>
using namespace std;
struct divisionByZero
{
	divisionByZero()
	{
		message = "Division by zero";
	}
	divisionByZero(string str)
	{
		message = str;
	}
	string what()
	{
		return message;
	}
	string message;
};

int main()
{
	int dividend, divisor, quotient;
	try
	{
		cout << "Line 3: Enter the dividend: ";
		cin >> dividend;
		cout << endl;
		cout << "Line 6: Enter the divisor: ";
		cin >> divisor;
		cout << endl;
		if (divisor == 0)
			throw divisionByZero();
		quotient = dividend / divisor;
		cout << "Line 12: Quotient = " << quotient
			<< endl;
	}
	catch (divisionByZero divByZeroObj)
	{
		cout << "Line 14: In the divisionByZero "
			<< "catch block: "
			<< divByZeroObj.what() << endl;
	}
	return 0;
}