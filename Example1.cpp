//Which one compiles successfully ? Explain.

struct Foo {};
struct Bar {};

int main(int argc, char** argv)
{
	Foo* f = new Foo;

	Bar* b1 = f;                              // (1)
	Bar* b2 = static_cast<Bar*>(f);           // (2)
	Bar* b3 = dynamic_cast<Bar*>(f);          // (3)
	Bar* b4 = reinterpret_cast<Bar*>(f);      // (4)
	Bar* b5 = const_cast<Bar*>(f);            // (5)

	return 0;
}