#include <iostream>
#include <cstring>

int main() {
	std::string str("A123456789");
	const char *cstr = str.c_str();
	char *nonconst_cstr = const_cast<char *> (cstr);
	*nonconst_cstr = 'B';
	std::cout << nonconst_cstr << std::endl;
	return 0;
}