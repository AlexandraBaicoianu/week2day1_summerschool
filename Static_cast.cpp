#include <vector>
#include <iostream>

enum class E { ONE = 1, TWO, THREE };
enum EU { ONE = 1, TWO, THREE };

int main()
{
	//0: first static_cast
	int p = 41;
	int q = 4;
	float m = p / q;
	float d = static_cast<float>(p) / q;
	std::cout << "m = " << m << std::endl;
	std::cout << "d = " << d << std::endl;

	// 1: initializing conversion
	int n = static_cast<int>(3.14);
	std::cout << "n = " << n << '\n';
	std::vector<int> v = static_cast<std::vector<int>>(10);
	std::cout << "v.size() = " << v.size() << '\n';

	// 2: lvalue to xvalue
	std::vector<int> v2 = static_cast<std::vector<int>&&>(v);
	std::cout << "after move, v.size() = " << v.size() << '\n';

	// 3: discarded-value expression
	static_cast<void>(v2.size());

	// 4: inverse of implicit conversion
	void* nv = &n;
	int* ni = static_cast<int*>(nv);
	std::cout << "*ni = " << *ni << '\n';

	// 5. scoped enum to int or float
	E e = E::ONE;
	int one = static_cast<int>(e);
	std::cout << one << '\n';

	// 6. int to enum, enum to another enum
	E e2 = static_cast<E>(one);
	EU eu = static_cast<EU>(e2);

	// 7. void* to any type
	void* voidp = &e;
	std::vector<int>* var= static_cast<std::vector<int>*>(voidp);

	return 0;
}