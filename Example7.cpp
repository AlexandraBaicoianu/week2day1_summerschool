#include <malloc.h>

/*
A pointer to any type of object can be assigned to a variable of type void*,
a void* can be assigned to another void*,
void* can be compared for equality and inequality,
and a void* can be explicitly converted to another type.
Other operations would be unsafe because the compiler cannot know what kind of object is really pointed to.
Consequently, other operations result in compile-time errors.
*/

int main()
{
	int* p = malloc(sizeof(*p));

	return 0;

}