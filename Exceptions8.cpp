#include <stdio.h>
#include <exception>
#include <chrono>
/*
Write a function that either returns a value or that throws that value based on
an argument. Measure the difference in run-time between the two ways
*/
struct IntReturnException : public std::exception {
	int storedInt;
	IntReturnException(int value) {
		storedInt = value;
	}
	const char * what() const throw () {
		return "Integer return exception";
	}
};
int function(int value, int arg = 0) {
	if (arg == 0) {
		return value;
	}
	else {
		throw IntReturnException(value);
	}
}
void main() {
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();
	function(2);
	end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end - start;
	printf("Normal return: %lf\n", elapsed_seconds.count());
	start = std::chrono::system_clock::now();
	try {
		function(2, 1);
	}
	catch (IntReturnException e) {
		end = std::chrono::system_clock::now();
		elapsed_seconds = end - start;
		printf("Exception return: %lf\n", elapsed_seconds.count());
	}
}
